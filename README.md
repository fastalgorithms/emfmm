# Maxwell FMM

FMM library for electromagnetic potentials

## Installation
The package has the following requirements, which should be placed in
the contrib/ directory:

- utilities v1.0 https://gitlab.com/fastalgorithms/utilities/tree/1-0-stable 
